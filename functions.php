<?php

if( !isset($_GET['page']) ){
	header('Location: ?page=medias');
}

define('CURRENT_FOLDER', getcwd());
define('DS', DIRECTORY_SEPARATOR);

function read_all_files($root = '.'){

    $files  = array('files'=>array(), 'dirs'=>array());
    $directories  = array();
    $last_letter  = $root[strlen($root)-1];
    $root  = ($last_letter == '\\' || $last_letter == '/') ? $root : $root.DIRECTORY_SEPARATOR;

    $directories[]  = $root;

    $ignoredFolders = array(
        $root . 'videos' . DIRECTORY_SEPARATOR,
        $root . 'team_photo' . DIRECTORY_SEPARATOR,
        $root . 'photo_oneshot' . DIRECTORY_SEPARATOR,
        $root . 'cache_team_photos' . DIRECTORY_SEPARATOR,
        $root . 'ALL' . DIRECTORY_SEPARATOR,
        $root . 'notepad' . DIRECTORY_SEPARATOR
    );

    while (sizeof($directories)) {
        $dir = array_pop($directories);
        if(in_array($dir, $ignoredFolders)){
            continue;
        }
        if ($handle = opendir($dir)) {
            while (false !== ($file = readdir($handle))) {
                $ext = pathinfo($file, PATHINFO_EXTENSION);
                if ($file == '.' || $file == '..' || $file == '.DS_Store' || $file == '.DS' || (strstr($file, '+++') && $_GET['page'] != 'admin') || in_array($ext, array('txt', 'json')) ) {
                    continue;
                }
                $file  = $dir.$file;
                if (is_dir($file)) {
                    $directory_path = $file.DIRECTORY_SEPARATOR;
                    array_push($directories, $directory_path);
                    if(!(in_array($directory_path, $ignoredFolders))) $files['challenges'][]  = str_replace('DATA'.DIRECTORY_SEPARATOR, '', $file);
                } elseif (is_file($file)) {
                    $files['files'][str_replace('DATA'.DIRECTORY_SEPARATOR, '', $dir)][]  = $file;
                    $files['files']['all'][]  = $file;
                }
            }
            closedir($handle);
        }
    }
    return $files;
}

function getTeamPhotos($root){
    $photos = array();
    $last_letter  = $root[strlen($root)-1];
    $root  = ($last_letter == '\\' || $last_letter == '/') ? $root : $root.DIRECTORY_SEPARATOR;

    $directories[]  = $root;

    while (sizeof($directories)) {
        $dir = array_pop($directories);
        if ($handle = opendir($dir)) {
            while (false !== ($file = readdir($handle))) {
                $file = $dir.$file;
                if(is_file($file)){
                    $tmp = explode('_', basename($file));
                    $photos[strtolower(trim($tmp[1]))] = $file;
                }
            }
            closedir($handle);
        }
    }
    return $photos;
}

function getTeamPoints($root){
    $points = array();
    $last_letter  = $root[strlen($root)-1];
    $root  = ($last_letter == '\\' || $last_letter == '/') ? $root : $root.DIRECTORY_SEPARATOR;

    $directories[]  = $root;

    while (sizeof($directories)) {
        $dir = array_pop($directories);
        if ($handle = opendir($dir)) {
            while (false !== ($file = readdir($handle))) {
                if ($file == '.' || $file == '..' || $file == '.DS_Store') {
                    continue;
                }
                $file = $dir.$file;
                if(is_file($file)){
                    $team = explode('_', basename($file));
                    $teamName = trim(strtolower(str_replace('notepad-', '', $team[0])));
                    if(filesize($file)){
                        $content = file_get_contents($file);
                        $content = explode("\n", $content);
                        foreach($content as $k => $line){
                            if(strlen($line)){
                                $tmp = explode(':', $line);
                                if(trim($tmp[0]) == 'Total'){
                                    $points[$teamName] = trim($tmp[1]);
                                }
                            }
                        }
                    } else {
                        $points[$teamName] = 0;
                    }

                }
            }
            closedir($handle);
        }
    }
    return $points;
}

function getNumberOfMediasByTeam($data){
    $values = array();
    $numberOfPhotos = 0;
    $numberOfHiddenPhotos = 0;
    $numberOfVideos = 0;
    $numberOfHiddenVideos = 0;
    foreach($data as $media){
        $ext = pathinfo(basename($media), PATHINFO_EXTENSION);
        if($ext == 'jpg'){
            $numberOfPhotos++;
            if(strstr(basename($media), '+++')){
                $numberOfHiddenPhotos = $numberOfHiddenPhotos + 1;
            }
        }
        if($ext == 'mp4'){
            $numberOfVideos++;
            if(strstr(basename($media), '+++')){
                $numberOfHiddenVideos = $numberOfHiddenVideos + 1;
            }
        }
    }
    $values['numberOfPhotos'] = $numberOfPhotos;
    $values['numberOfHiddenPhotos'] = $numberOfHiddenPhotos;
    $values['numberOfVideos'] = $numberOfVideos;
    $values['numberOfHiddenVideos'] = $numberOfHiddenVideos;
    return $values;
}

$user_agent = $_SERVER['HTTP_USER_AGENT'];
function getOS() {

    global $user_agent;

    $os_platform    =   "Unknown OS Platform";

    $os_array       =   array(
    '/windows nt 6.3/i'     =>  'Windows 8.1',
    '/windows nt 6.2/i'     =>  'Windows 8',
    '/windows nt 6.1/i'     =>  'Windows 7',
    '/windows nt 6.0/i'     =>  'Windows Vista',
    '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
    '/windows nt 5.1/i'     =>  'Windows XP',
    '/windows xp/i'         =>  'Windows XP',
    '/windows nt 5.0/i'     =>  'Windows 2000',
    '/windows me/i'         =>  'Windows ME',
    '/win98/i'              =>  'Windows 98',
    '/win95/i'              =>  'Windows 95',
    '/win16/i'              =>  'Windows 3.11',
    '/macintosh|mac os x/i' =>  'Mac OS X',
    '/mac_powerpc/i'        =>  'Mac OS 9',
    '/linux/i'              =>  'Linux',
    '/ubuntu/i'             =>  'Ubuntu',
    '/iphone/i'             =>  'iPhone',
    '/ipod/i'               =>  'iPod',
    '/ipad/i'               =>  'iPad',
    '/android/i'            =>  'Android',
    '/blackberry/i'         =>  'BlackBerry',
    '/webos/i'              =>  'Mobile'
    );

    foreach ($os_array as $regex => $value) {
        if (preg_match($regex, $user_agent)) {
            $os_platform = $value;
        }
    }

    return $os_platform;

}
