<?php
include_once('functions.php');
$data = read_all_files('DATA');
$teamPhotos = getTeamPhotos('DATA' . DS . 'team_photo');
$teamPoints = getTeamPoints('DATA' . DS . 'notepad');
$imagesExt = array('jpg', 'png', 'bmp', 'tiff');
$videosExt = array('mp4', 'ogg');
$filesPerTeam = array();
foreach($data['files']['all'] as $file){
	$tmp = explode('_', $file);
	if(sizeof($tmp)>1) $team = trim(strtolower(trim($tmp[1])));
	$ext = pathinfo($file, PATHINFO_EXTENSION);
	if(!strstr($file, 'notepad'))$filesPerTeam[$team][] = $file;
}
unset($filesPerTeam['tablet']);
unset($filesPerTeam['team']);
unset($filesPerTeam['photo/team']);
unset($filesPerTeam['oneshot/offchallenge']);
unset($filesPerTeam['cache_team_photos']);
ksort($filesPerTeam);

$admin = ($_GET['page'] == 'admin') ? true : false;
$root = CURRENT_FOLDER . DS;

?>
<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="Denis Bossy">

		<title>Urbangaming Debriefing</title>

		<link href="cyborg/bootstrap.min.css" rel="stylesheet">

		<link rel="stylesheet" type="text/css" href="lightview/css/lightview/lightview.css"/>
		<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">

		<style>
			body {
				padding-top: 85px;
			}
			.well {
				background-color: #222;
				padding: 5px;
			}
			.well img, .well video {
				 border: 0px solid #fff;
			}
			.well h3 {
				padding: 0;
				margin: 5px 0;
			}
			.label-success, .label-danger {
				font-weight: normal;
			}
			.html5lightbox {
				display: block;
				position: relative;
			}
			.html5lightbox .glyphicon {
				font-size: 48px;
				position: absolute;
				left: 50%;
				top: 50%;
				margin-left: -24px;
				margin-top: -24px;
				color: #fff;
			}
			.html5lightbox .shadow {
				width: 100%;
				height: 100%;
				position: absolute;
				left: 0;
				top: 0;
				bottom: 0;
				right: 0;
				moz-box-shadow: inset 0px 0px 40px 0px #000;
				-webkit-box-shadow: inset 0px 0px 40px 0px #000;
				-o-box-shadow: inset 0px 0px 40px 0px #000;
				box-shadow: inset 0px 0px 40px 0px #000;
				filter:progid:DXImageTransform.Microsoft.Shadow(color=#000, Direction=NaN, Strength=40);
			}
			h1, h2, h3, h4, h5, h6, .lv_title, #html5-text {
				color: #ffbf08;
				text-transform: uppercase;
			}
			.lv_title {
				font-size: 20px;
				margin-bottom: 5px;
			}
			.lv_urbangaming {
				position: relative;
			}
			.lv_urbangaming .logo {
				position: absolute;
				opacity: 0.75;
				width: 200px;
				height: 36px;
				right: 10px;
				bottom: 10px;
				background: url('logo.png') 0px 0px no-repeat transparent;
			}
			.caption {
				position: relative;
			}
			.caption .label, .lv_caption .label {
				display: none;
			}
			.navbar {
				height: 65px;
			}
			table#standing tbody tr td .btn-group button {
				display: none;
			}
			table#standing tbody tr:hover td .btn-group button {
				display: block;
			}
			table#standing tbody tr:first-child {
				font-weight: bold;
				color: #ffbf08;
			}
			table#standing tr th, table#standing tr td {
				font-size: 1.6em;
			}
			table#standing tr td.team {
				text-transform: uppercase;
				vertical-align: middle;
				font-size: 2em;
			}
			table#standing tr td.team img{
				margin-right: 20px;
			}
			#html5-watermark {
				display: none !important;
			}
			#html5-elem-wrap, #html5-elem-box, #html5-elem-data-box, #html5-text {
				background-color: transparent !important;
			}
			#html5-text {
				font-size: 18px !important;
				color: #ffbf08 !important;
				text-transform: uppercase !important;
				font-weight: 500 !important;
			}
		</style>
	</head>

	<body>
		<div class="container-fluid">
			<div class="navbar navbar-inverse navbar-fixed-top">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="?page=medias" style="">
						<?php if(file_exists("CUSTOM/logo.png")): ?>
						<img src="CUSTOM/logo.png" height="36px">
						<?php else: ?>
						<img src="logo.png" height="36px">
						<?php endif; ?>
					</a>
				</div>
				<div class="navbar-collapse collapse navbar-inverse-collapse navbar-right">
					<ul class="nav navbar-nav">
						<li class="<?php print ($_GET['page'] == 'medias') ? 'active' : ''; ?>"><a href="?page=medias"><i class="fa fa-picture-o fa-2x"></i></a></li>
						<li class="<?php print ($_GET['page'] == 'standings') ? 'active' : ''; ?>"><a href="?page=standings"><i class="fa fa-trophy fa-2x"></i></a></li>
						<li class="<?php print ($_GET['page'] == 'admin') ? 'active' : ''; ?>"><a href="?page=admin&direction=challenges"><i class="fa fa-cogs fa-2x"></i></a></li>
					</ul>
				</div>
			</div>

			<?php if(isset($_GET['page'])): ?>

			<?php if($_GET['page'] == 'medias' || $_GET['page'] == 'admin'): ?>

			<?php if($admin): //print_r($data) ?>
			<div id="teams" class="row" style="margin-bottom: 20px">
				<div class="well well-sm">
					<h3>Equipes</h3>
				</div>
				<div class="col-md-12">
					<p>Entrez les points de chaque équipe avant de passer au debriefing.</p>
				</div>
				<div class="col-md-12">
					<form class="form-horizontal" role="form" id="teams">
					<table class="table">
					<thead>
						<tr>
							<th>Equipe</th>
							<th>Points</th>
							<th class="text-center">Nombre de photos</th>
							<th class="text-center">Photos affichées</th>
							<th class="text-center">Photos masquées</th>
							<th class="text-center">Nombre de vidéos</th>
							<th class="text-center">Vidéos affichées</th>
							<th class="text-center">Vidéos masquées</th>
						</tr>
					</thead>
					<?php foreach($filesPerTeam as $team => $item): $values = getNumberOfMediasByTeam($filesPerTeam[$team]); ?>
						<?php if(!empty($team)): ?>
						<tr>
							<td><?php echo $team; ?></td>
							<td>
								<input type="number" class="form-control input-sm" data-team="<?php echo $team; ?>" name="teams[<?php echo $team; ?>]" value="<?php echo $teamPoints[$team]; ?>">
							</td>
							<td class="text-center"><?php echo $values['numberOfPhotos']; ?></td>
							<td class="text-center"><?php echo $values['numberOfPhotos'] - $values['numberOfHiddenPhotos']; ?></td>
							<td class="text-center"><?php echo $values['numberOfHiddenPhotos']; ?></td>
							<td class="text-center"><?php echo $values['numberOfVideos']; ?></td>
							<td class="text-center"><?php echo $values['numberOfVideos'] - $values['numberOfHiddenVideos']; ?></td>
							<td class="text-center"><?php echo $values['numberOfHiddenVideos']; ?></td>
						</tr>
						<?php endif; ?>
					<?php endforeach; ?>
					<tr>
						<td colspan="7" class="text-center">
							<button type="submit" class="btn btn-success btn-sm">Sauvegarder les points</button>
							<span class="help-text text-success" id="teamsSuccess" style="display: none">Les points ont été sauvegardés!</span>
							<button type="reset" class="btn btn-danger btn-sm">Remettre à zéro</button>
						</td>
					</tr>
					</table>
					</form>
				</div>
			</div>
			<?php endif; ?>

			<?php if($_GET['page'] == 'admin'): ?>
			<div class="row" style="margin-bottom: 20px">
				<div class="col-md-12">
					<div class="btn-group">
						<a href="?page=admin&direction=challenges" class="btn btn-default<?php if($_GET['direction'] == 'challenges'): ?> active<?php endif; ?>">Afficher les médias par challenge</a>
						<a href="?page=admin&direction=teams" class="btn btn-default<?php if($_GET['direction'] == 'teams'): ?> active<?php endif; ?>">Afficher les médias par équipe</a>
					</div>
				</div>
			</div>
			<?php endif; ?>

			<div id="challenges">
			<?php if(isset($_GET['direction']) && $_GET['direction'] == 'teams'): ?>
			<?php foreach($filesPerTeam as $team => $files): ?>
				<div class="well well-sm">
					<h3><?php print $team; ?></h3>
				</div>
				<div class="row">
			<?php foreach($files as $file): ?>
				<?php
					$tmp = explode('_', $file);
					$tmp1 = explode('/', $tmp[0]);
					$team = $tmp[1];
					$ext = pathinfo($file, PATHINFO_EXTENSION);
					$ignored = strstr($file, '+++') ? true : false;
					$challenge = $tmp1[1];
				?>
				<div class="col-md-3 col-sm-4 col-lg-3 col-xs-12">
					<div class="well"<?php if($ignored) : ?>style="opacity:0.3"<?php endif; ?>>
						<?php
						$buttons = "<div class='btn-group'>";
						$buttons .= "<button type='button' class='btn btn-default btn-xs' data-team='$team' data-op='minus'>";
						$buttons .= "<i class='fa fa-minus'></i></button>";
						$buttons .= "<button type='button' class='btn btn-default btn-xs' data-team='$team' data-op='plus'>";
						$buttons .= "<i class='fa fa-plus'></i></button></div>";
						$buttons .= "<span class='label label-success'>+1</span><span class='label label-danger'>-1</span>";
						$buttons .= "<script type='text/javascript'>";
						$buttons .= "$('.lv_caption button').click(function(e){
									var team = $(this).data('team');
									if(typeof team == 'string'){
										team = team.toLowerCase();
									}
									var op = $(this).data('op');

									if(window.localStorage.getItem(team)){
										var actualPoints = parseInt(window.localStorage[team]);
									} else {
										var actualPoints = 0;
									}

									if(op == 'plus'){
										var newPoints = actualPoints + 1;
										$(this).parents('.lv_caption').find('.label-success').fadeIn(50).delay(500).fadeOut(200);
									}
									if(op == 'minus'){
										var newPoints = actualPoints - 1;
										$(this).parents('.lv_caption').find('.label-danger').fadeIn(50).delay(500).fadeOut(200);
									}
									if(op == 'reset'){
										var newPoints = 0;
									}
									window.localStorage.setItem(team, newPoints);})";
						$buttons .= "</script>";
						?>
						<?php if(in_array($ext, $imagesExt)): ?>
							<a href="<?php print $file; ?>" class="html5lightbox" data-group="<?php print $challengeName; ?>" title="<?php print $team; ?>">
								<span class="glyphicon glyphicon-camera"></span>
								<img src="<?php print $file; ?>" class="img-responsive">
								<div class="shadow"></div>
							</a>
						<?php elseif(in_array($ext, $videosExt)): $rand=rand(1,1000); ?>
							<?php
							$os = getOS();
							$pos = (strpos($os, "Mac"));
							if(is_numeric($pos)){
								$fname = basename($root . $file, ".mp4");
								$video_poster = "thumbs" . DS .($fname).'.jpg';
								if(!file_exists($video_poster)){
									$ffmpeg = $root . "bin" . DS . "ffmpeg";
									$ouput_path = $root . 'thumbs' . DS;
									$thmb = $ouput_path.($fname).'.jpg';
									$thmburl = str_replace("'", "", escapeshellarg($thmb));
									$cmd = "$ffmpeg -i '$root".str_replace("'", "", escapeshellarg($file))."' -an -y -f mjpeg -ss 00:00:04 -vframes 1 '".$thmburl."'";
									$stat = system(($cmd));
									$cmd2 = "chmod -R 777 ./";
									system($cmd2);
									$video_poster = "thumbs/".($fname).'.jpg';
								}
							} else {
								$fname = basename($root . $file, ".mp4");
								$video_poster = "thumbs" . DS . ($fname).'.jpg';
								if(!file_exists($video_poster)){
									$ffmpeg = $root . "bin".DS."ffmpeg";
									$ouput_path = $root . 'thumbs' . DS;
									$thmb = $ouput_path.($fname).'.jpg';
									$thmburl = str_replace("'", "", escapeshellarg($thmb));
									$cmd = "$ffmpeg.exe -i \"$root".$file."\" -an -y -f mjpeg -ss 00:00:04 -vframes 1 ".$thmburl."";
									$stat = system(($cmd));
									$cmd2 = "chmod -R 777 ./";
									system($cmd2);
								}
							}

							if(!filesize($video_poster)){
								$video_poster = $teamPhotos[strtolower($team)]; // mettre photo équipe
							}
							?>
							<?php
							$pathinfo = pathinfo($file);
							$tmp = explode('_', $pathinfo['filename']);
							$dimensions = (end($tmp) == 'ratio') ? 'height:1600,width:900' : 'width:1600,height:900';
							?>
							<a class="html5lightbox" title="<?php print $team; ?>"
								href="<?php print $file; ?>"
								data-group="<?php print $challengeName; ?>">
								<span class="glyphicon glyphicon-play"></span>
								<img src="<?php echo $video_poster; ?>" alt="Video" title="Lire la vidéo" class="img-responsive">
								<div class="shadow"></div>
								<div id="video<?php print $rand; ?>" style="display:none">
									<video controls preload="none" poster="<?=$video_poster?>">
										<source src="<?php print $file; ?>" type="video/mp4">
									</video>
								</div>
							</a>
						<?php endif; ?>
						<div class="caption">
							<h4><?php print $challenge ;?></h4>
							<?php if($admin): ?>
								<?php if($ignored): ?>
								<a href="#" class="btn btn-default btn-xs btn-block showMedia" data-file="<?php echo $file; ?>"><i class="fa fa-eye">&nbsp; Afficher</i></a>
								<?php else: ?>
								<a href="#" class="btn btn-default btn-xs btn-block hideMedia" data-file="<?php echo $file; ?>"><i class="fa fa-eye-slash">&nbsp; Masquer</i></a>
								<?php endif; ?>
							<?php else: ?>
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-xs" data-team="<?php print $team; ?>" data-op="minus"><i class="fa fa-minus"></i></button>
								<button type="button" class="btn btn-default btn-xs" data-team="<?php print $team; ?>" data-op="plus"><i class="fa fa-plus"></i></button>
							</div>
							<?php endif; ?>
							<span class="label label-success">+1</span>
							<span class="label label-danger">-1</span>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
			</div>
			<?php endforeach; ?>
			<?php else: ?>
			<?php foreach($data['challenges'] as $chKey => $challengeName): $challengeKey = $challengeName . DS ?>
				<?php if($challengeName != 'notepad' && sizeof($data['files'][$challengeKey]) > 0): ?>
				<?php if(!$admin) unset($data['files'][$challengeKey]['ignored']); ?>
				<div class="well well-sm">
				<h3><?php print $challengeName; ?></h3>
				</div>
				<div class="row">
				<?php foreach($data['files'][$challengeKey] as $fKey => $file): ?>
				<?php
					$tmp = explode('_', $file);
					$team = $tmp[1];
					$ext = pathinfo($file, PATHINFO_EXTENSION);
					$ignored = strstr($file, '+++') ? true : false;
				?>
					<div class="col-md-3 col-sm-4 col-lg-3 col-xs-12">
						<div class="well"<?php if($ignored) : ?>style="opacity:0.3"<?php endif; ?>>
							<?php
							$buttons = "<div class='btn-group'>";
							$buttons .= "<button type='button' class='btn btn-default btn-xs' data-team='$team' data-op='minus'>";
							$buttons .= "<i class='fa fa-minus'></i></button>";
							$buttons .= "<button type='button' class='btn btn-default btn-xs' data-team='$team' data-op='plus'>";
							$buttons .= "<i class='fa fa-plus'></i></button></div>";
							$buttons .= "<span class='label label-success'>+1</span><span class='label label-danger'>-1</span>";
							$buttons .= "<script type='text/javascript'>";
							$buttons .= "$('.lv_caption button').click(function(e){
										var team = $(this).data('team');
										if(typeof team == 'string'){
											team = team.toLowerCase();
										}
										var op = $(this).data('op');

										if(window.localStorage.getItem(team)){
											var actualPoints = parseInt(window.localStorage[team]);
										} else {
											var actualPoints = 0;
										}

										if(op == 'plus'){
											var newPoints = actualPoints + 1;
											$(this).parents('.lv_caption').find('.label-success').fadeIn(50).delay(500).fadeOut(200);
										}
										if(op == 'minus'){
											var newPoints = actualPoints - 1;
											$(this).parents('.lv_caption').find('.label-danger').fadeIn(50).delay(500).fadeOut(200);
										}
										if(op == 'reset'){
											var newPoints = 0;
										}
										window.localStorage.setItem(team, newPoints);})";
							$buttons .= "</script>";
							?>
							<?php if(in_array($ext, $imagesExt)): ?>
								<a href="<?php print $file; ?>" class="html5lightbox" data-group="<?php print $challengeName; ?>" title="<?php print $team; ?>">
									<span class="glyphicon glyphicon-camera"></span>
									<img src="<?php print $file; ?>" class="img-responsive">
									<div class="shadow"></div>
								</a>
							<?php elseif(in_array($ext, $videosExt)): $rand=rand(1,1000); ?>
								<?php
								$os = getOS();
								$pos = (strpos($os, "Mac"));
								if(is_numeric($pos)){
									$fname = basename($root . $file, ".mp4");
									$video_poster = "thumbs" . DS .($fname).'.jpg';
									if(!file_exists($video_poster)){
										$ffmpeg = $root . "bin" . DS . "ffmpeg";
										$ouput_path = $root . 'thumbs' . DS;
										$thmb = $ouput_path.($fname).'.jpg';
										$thmburl = str_replace("'", "", escapeshellarg($thmb));
										$cmd = "$ffmpeg -i '$root".str_replace("'", "", escapeshellarg($file))."' -an -y -f mjpeg -ss 00:00:04 -vframes 1 '".$thmburl."'";
										$stat = system(($cmd));
										$cmd2 = "chmod -R 777 ./";
										system($cmd2);
										$video_poster = "thumbs/".($fname).'.jpg';
									}
								} else {
									$fname = basename($root . $file, ".mp4");
									$video_poster = "thumbs" . DS . ($fname).'.jpg';
									if(!file_exists($video_poster)){
										$ffmpeg = $root . "bin".DS."ffmpeg";
										$ouput_path = $root . 'thumbs' . DS;
										$thmb = $ouput_path.($fname).'.jpg';
										$thmburl = str_replace("'", "", escapeshellarg($thmb));
										$cmd = "$ffmpeg.exe -i \"$root".$file."\" -an -y -f mjpeg -ss 00:00:04 -vframes 1 ".$thmburl."";
										$stat = system(($cmd));
										$cmd2 = "chmod -R 777 ./";
										system($cmd2);
									}
								}

								if(!filesize($video_poster)){
									$video_poster = $teamPhotos[strtolower($team)]; // mettre photo équipe
								}
								?>
								<?php
								$pathinfo = pathinfo($file);
								$tmp = explode('_', $pathinfo['filename']);
								$dimensions = (end($tmp) == 'ratio') ? 'height:1600,width:900' : 'width:1600,height:900';
								?>
								<a class="html5lightbox" title="<?php print $team; ?>"
									href="<?php print $file; ?>"
									data-group="<?php print $challengeName; ?>">
									<span class="glyphicon glyphicon-play"></span>
									<img src="<?php echo $video_poster; ?>" alt="Video" title="Lire la vidéo" class="img-responsive">
									<div class="shadow"></div>
									<div id="video<?php print $rand; ?>" style="display:none">
										<video controls preload="none" poster="<?=$video_poster?>">
											<source src="<?php print $file; ?>" type="video/mp4">
										</video>
									</div>
								</a>
							<?php endif; ?>
							<div class="caption">
								<h4><?php print $team ;?></h4>
								<?php if($admin): ?>
									<?php if($ignored): ?>
									<a href="#" class="btn btn-default btn-xs btn-block showMedia" data-file="<?php echo $file; ?>"><i class="fa fa-eye">&nbsp; Afficher</i></a>
									<?php else: ?>
									<a href="#" class="btn btn-default btn-xs btn-block hideMedia" data-file="<?php echo $file; ?>"><i class="fa fa-eye-slash">&nbsp; Masquer</i></a>
									<?php endif; ?>
								<?php else: ?>
								<div class="btn-group">
									<button type="button" class="btn btn-default btn-xs" data-team="<?php print $team; ?>" data-op="minus"><i class="fa fa-minus"></i></button>
									<button type="button" class="btn btn-default btn-xs" data-team="<?php print $team; ?>" data-op="plus"><i class="fa fa-plus"></i></button>
								</div>
								<?php endif; ?>
								<span class="label label-success">+1</span>
								<span class="label label-danger">-1</span>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
				</div>
				<?php endif; ?>
			<?php endforeach; ?>
			<?php endif; ?>
			</div>

			<?php elseif($_GET['page'] == 'standings'): ?>

			<h2>Classement</h2>

			<table class="table table-striped" id="standing">
				<thead>
					<tr>
						<th style="width: 40px">#</th>
						<th style="">Equipe</th>
						<th>Points</th>
						<!--th>&nbsp;</th-->
					</tr>
				</thead>
				<tbody>
					<?php $i=1; foreach($filesPerTeam as $teamName => $file): ?>
					<tr data-point="" data-team="<?php print $teamName ?>">
						<td class="position"><?php print $i; ?></td>
						<td class="team">
							<img src="<?php echo $teamPhotos[strtolower($teamName)]; ?>" width="200px">
							<?php print $teamName; ?>
						</td>
						<td class="point"></td>
						<!--td>
							<div class="btn-group">
								<button type="button" class="btn btn-default btn-xs" data-team="<?php print $teamName; ?>" data-op="reset"><span class="glyphicon glyphicon-repeat"></span></button>
								<button type="button" class="btn btn-default btn-xs" data-team="<?php print $teamName; ?>" data-op="minus"><span class="glyphicon glyphicon-minus"></span></button>
								<button type="button" class="btn btn-default btn-xs" data-team="<?php print $teamName; ?>" data-op="plus"><span class="glyphicon glyphicon-plus"></span></button>
							</div>
						</td-->
					</tr>
					<?php $i++; endforeach; ?>
				</tbody>
			</table>
			<?php endif; ?>

			<?php endif; ?>

		</div>
	    <!-- Bootstrap core JavaScript
	    ================================================== -->
	    <!-- Placed at the end of the document so the pages load faster -->
	    <script src="js/jquery.min.js"></script>
	    <script type="text/javascript" src="js/swfobject.js"></script>
	    <script src="js/bootstrap.min.js"></script>

	    <script type="text/javascript" src="lightview/js/spinners/spinners.min.js"></script>
		<script type="text/javascript" src="lightview/js/lightview/lightview.js"></script>
		<script type="text/javascript" src="html5lightbox/html5lightbox.js"></script>

		<script type="text/javascript" src="js/jquery.sortElements.js"></script>

		<script type="text/javascript">

		var animateStanding = function(table){
			var lines = table.find('tbody tr');
			var numberOfLines = lines.length;
			lines.hide();

			var lastIndex = numberOfLines;

			$('body').on('keydown', function(e){
				if(e.which == 39){
					if(lastIndex >= 0){
						var lastLine = $(lines[lastIndex - 1]);
						lastLine.fadeIn('slow');
						lastIndex--;
					}
				}
				if(e.which == 37){
					if(lastIndex >= 0){
						var lastLine = $(lines[lastIndex]);
						lastLine.fadeOut('fast');
						lastIndex++;
					}
				}
			});

			// setInterval(function(){
			// 	if(lastIndex >= 0){
			// 		var lastLine = $(lines[lastIndex - 1]);
			// 		lastLine.fadeIn('slow');
			// 		lastIndex--;
			// 	}
			// }, 2500)

		}

			$(document).ready(function(){

				function sortTable(table, callback){
					var rows = table.find('tbody tr').get();
					rows.sort(function(a, b) {
						var A = $(a).data('point');
						var B = $(b).data('point');
						if(A < B) {
							return 1;
						}
						if(A > B) {
							return -1;
						}
						return 0;
					});
					$.each(rows, function(index, row) {
						table.children('tbody').append(row);
					});
					//table.find('tbody tr:first').addClass('text-success').find('td').css('font-weight: bold');
					callback();
				}

				$('#sort button').click(function(event){
					var href = $(this).data('href');
					document.location.href = href;
				})

				$('#standing button, .caption .btn-group button, .lv_caption button').click(function(event){

					event.preventDefault();

					var team = $(this).data('team');
					var op = $(this).data('op');
					if(typeof team == 'string'){
						team = team.toLowerCase();
					}

					if(window.localStorage.getItem(team)){
						var actualPoints = parseInt(window.localStorage[team]);
					} else {
						var actualPoints = 0;
					}

					if(op == 'plus'){
						var newPoints = actualPoints + 1;
						$(this).parents('.caption').find('.label-success').fadeIn(50).delay(500).fadeOut(200);
					}
					if(op == 'minus'){
						var newPoints = actualPoints - 1;
						$(this).parents('.caption').find('.label-danger').fadeIn(50).delay(500).fadeOut(200);
					}
					if(op == 'reset'){
						var newPoints = 0;
					}
					window.localStorage.setItem(team, newPoints);

					$('tr[data-team="'+team+'"] td.point').text(newPoints);
					$('tr[data-team="'+team+'"]').attr('data-point', newPoints);

					/*sortTable($('table#standing'), function(){
						$('table#standing tbody tr').each(function(i,e){
							$(e).find('td.position').text(i+1);
						})
					});*/

				})

				if($('table#standing').length){
					$('table#standing tbody tr').each(function(i,e){
						$(e).find('td.point').text(window.localStorage[$(e).data('team')]);
						$(e).attr('data-point', window.localStorage[$(e).data('team')]);
					})

					sortTable($('table#standing'), function(){
						$('table#standing tbody tr').each(function(i,e){
							$(e).find('td.position').text(i+1);
						})
					});
					animateStanding($('table#standing'));

				}

				$('.hideMedia').click(function(event){
					event.preventDefault();
					var container = $(this).parents('.well');
					var uri = $(this).attr('data-file');
					var tmp = uri.split('<?php print DS; ?>');
					var filename = tmp[tmp.length-1];
					$.ajax({
						url: 'ajax.php',
						type: 'post',
						data: {
							uri: uri,
							mode: 'hide',
							function: 'renameMedia'
						},
						success: function(data){
							container.stop().animate({
								'opacity': 0.3
							}, 'fast');
							container.find('.caption a:first').removeClass('hideMedia').addClass('showMedia').html('<i class="fa fa-eye">&nbsp; Afficher</i>');
						}
					})
				})

				$('.showMedia').click(function(event){
					event.preventDefault();
					var container = $(this).parents('.well');
					var uri = $(this).attr('data-file');
					var tmp = uri.split('<?php print DS; ?>');
					var filename = tmp[tmp.length-1];
					$.ajax({
						url: 'ajax.php',
						type: 'post',
						data: {
							uri: uri,
							mode: 'show',
							function: 'renameMedia'
						},
						success: function(data){
							container.stop().animate({
								'opacity': 1
							}, 'fast');
							container.find('.caption a:first').removeClass('showMedia').addClass('hideMedia').html('<i class="fa fa-eye-slash">&nbsp; Masquer</i>');
						}
					})
				})

				$('body').on('click', '.changeRatioMedia', function(event){
					event.preventDefault();
					var uri = $(this).data('file');
					var $this = $(this);
					$.ajax({
						url: 'ajax.php',
						type: 'post',
						dataType: 'json',
						data: {
							uri: uri,
							function: 'changeRatioMedia'
						},
						success: function(data){
							$this.data('file', data.newUri);
						}
					})
				});

				$('form#teams').submit(function(e){
					e.preventDefault();
					$('form#teams input').attr('disabled', 'disabled');
					$('form#teams input').each(function(i,e){
						var points = $(e).val();
						var team = $(e).data('team');
						if(typeof team == 'string'){
							team = team.toLowerCase();
						}
						window.localStorage.setItem(team, points);
						$(e).removeAttr('disabled');
					});
					$('#teamsSuccess').fadeIn().delay(1000).fadeOut();
				});
				if($('form#teams').length){
					$('form#teams input').each(function(i,e){
						if(window.localStorage[$(e).data('team')]){
							$(e).val(window.localStorage[$(e).data('team')]);
						}
					});
				}

				$('button').tooltip();

			})

		</script>
	</body>
</html>
